package pages;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import pageElements.LoginPageElements;

public class LoginPage extends BasePage {

	WebDriver driver;
	LoginPageElements loginPage;
	
	public LoginPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		loginPage = new LoginPageElements(this.driver);
		
	}

	
	public void enterUserName(String UsName) {
		
		loginPage.username.sendKeys(UsName);
	}
	
	public void enterPassword(String pw) throws UnknownHostException {
		loginPage.password.sendKeys(pw);
		System.out.println("IIPPP ADD===="+InetAddress.getLocalHost().getHostAddress());
	}
	public void clickSubmit() {
		loginPage.submit.click();
	}
	public boolean verifyProfileText(String expectedText) {
		return wait.until(ExpectedConditions.attributeContains(loginPage.profileText,"innerText", expectedText));
	}
	
	
}
